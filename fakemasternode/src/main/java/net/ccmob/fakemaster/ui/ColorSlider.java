package net.ccmob.fakemaster.ui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

public class ColorSlider extends JFrame {

	private JPanel		contentPane;

	private JSlider		red		= new JSlider();
	private JSlider		green	= new JSlider();
	private JSlider		blue	= new JSlider();
	private CoreFrame	frame;

	/**
	 * Create the frame.
	 */
	public ColorSlider(CoreFrame f) {
		this.frame = f;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 262, 141);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(this.contentPane);
		this.red.setMaximum(10);
		this.red.setValue(0);

		this.red.addChangeListener(e -> {
			this.update();
		});
		this.green.setMaximum(10);
		this.green.setValue(0);

		this.green.addChangeListener(e -> {
			this.update();
		});
		this.blue.setMaximum(10);
		this.blue.setValue(0);

		this.blue.addChangeListener(e -> {
			this.update();
		});

		GroupLayout gl_contentPane = new GroupLayout(this.contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(this.red, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(this.green, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(this.blue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap(214, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addComponent(this.red, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.green, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.blue, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE).addContainerGap(177, Short.MAX_VALUE)));
		this.contentPane.setLayout(gl_contentPane);

		this.setVisible(true);

	}

	public void update() {
		NodePacket p = new NodePacket((byte) 10, 0);
		p.getData().add(this.red.getValue() * 10);
		p.getData().add(this.green.getValue() * 10);
		p.getData().add(this.blue.getValue() * 10);
		p.send(this.frame.serialPort);
	}
}
