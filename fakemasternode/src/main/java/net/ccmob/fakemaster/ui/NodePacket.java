package net.ccmob.fakemaster.ui;

import java.util.ArrayList;

import jssc.SerialPort;
import jssc.SerialPortException;

public class NodePacket {

	private static final byte		startByte	= '#';
	private static final byte		stopByte	= '*';

	private byte								cmd;
	private int									address;
	private ArrayList<Integer>	data;

	public NodePacket(byte cmd, int address) {
		this.cmd = cmd;
		this.address = address;
		this.data = new ArrayList<>();
	}

	public void send(SerialPort p) {
		int cmdValue = this.cmd;
		int addressValue = this.address;
		int[] buffer = new int[9 + this.data.size()];
		byte[] bytes = new byte[buffer.length];
		buffer[0] = startByte;
		int size = this.data.size() + 3;
		buffer[1] = ((size & 0xFF00) >> 8);
		buffer[2] = (size & 0x00FF);
		buffer[3] = ((addressValue & 0xFF00) >> 8);
		buffer[4] = (addressValue & 0x00FF);
		buffer[5] = (cmdValue & 0xFF);
		int i = 0;
		for (int d : this.data) {
			buffer[6 + i] = (d & 0xFF);
			i++;
		}
		buffer[this.data.size() + 6] = stopByte;
		int crc = this.genCRC16_ccitt(cmdValue, addressValue) & 0xFFFF;
		int crc1 = (crc & 0xFF00) >> 8;
		int crc2 = (crc & 0x00FF);
		buffer[this.data.size() + 6 + 1] = crc1;
		buffer[this.data.size() + 6 + 2] = crc2;

		System.out.println("Bytes:");
		i = 0;
		for (int v : buffer) {
			System.out.println(String.format("0x%02X", v & 0xFF));
			bytes[i++] = (byte) (v & 0xFF);
		}

		try {
			p.writeBytes(bytes);
		} catch (SerialPortException e1) {
			e1.printStackTrace();
		}
	}

	private int genCRC16_ccitt(int cmd, int address) {
		byte[] buffer = new byte[this.data.size() + 3];
		buffer[0] = (byte) (((address & 0xFF00) >> 8) & 0xFF);
		buffer[1] = (byte) (address & 0x00FF);
		buffer[2] = (byte) (cmd & 0x00FF);
		for (int i = 0; i < this.data.size(); i++) {
			buffer[i + 3] = (byte) (this.data.get(i) & 0xFF);
		}
		int crc = 0x00;
		for (byte b : buffer) {
			byte data = b;
			int i;
			crc = crc ^ (data << 8);
			for (i = 0; i < 8; i++) {
				if ((crc & 0x8000) != 0) {
					crc = (crc << 1) ^ 0x1021;
				} else {
					crc <<= 1;
				}
			}
		}
		return crc;
	}

	public ArrayList<Integer> getData() {
		return this.data;
	}

	public void setData(ArrayList<Integer> data) {
		this.data = data;
	}

}
