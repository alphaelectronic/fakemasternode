package net.ccmob.fakemaster.ui;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import jssc.SerialPortException;

public class CommandConstructor extends JFrame implements WindowListener {

	private JPanel										contentPane;
	private JTextField								hexValue;
	private ArrayList<Integer>				data				= new ArrayList<>();
	private CoreFrame									frame;
	private JLabel										lblCommand	= new JLabel("Command");
	private JSpinner									cmd					= new JSpinner();
	private JLabel										lblAdress		= new JLabel("Adress");
	private JSpinner									addr				= new JSpinner();
	private JList<String>							list				= new JList<>();
	private JButton										btnAddHex		= new JButton("Add Hex");
	private JSpinner									intValue		= new JSpinner();
	private final JButton							btnRemove		= new JButton("Remove");
	private DefaultListModel<String>	model				= new DefaultListModel<>();

	/**
	 * Create the frame.
	 */
	public CommandConstructor(CoreFrame frame) {
		this.frame = frame;
		this.setResizable(false);
		this.addWindowListener(this);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 501, 300);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(this.contentPane);

		this.hexValue = new JTextField();
		this.hexValue.setColumns(10);

		this.btnAddHex.addActionListener(arg0 -> {
			String val = this.hexValue.getText();
			if (val.startsWith("0x")) {
				val = val.substring(2);
			}
			int v = (int) Long.parseLong(val, 16);
			if (v > 255) {
				return;
			}
			this.data.add(v);
			this.model.addElement(String.format("0x%02X", v));
		});

		this.list.setModel(this.model);

		JButton btnAddInt = new JButton("Add Int");
		btnAddInt.addActionListener(e -> {
			this.data.add((int) this.intValue.getValue());
			this.model.addElement(String.format("0x%02X", (int) this.intValue.getValue()));
		});

		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(e -> {
			NodePacket p = new NodePacket((byte) (int) this.cmd.getValue(), (int) this.addr.getValue());
			p.getData().addAll(this.data);
			p.send(frame.serialPort);
		});

		GroupLayout gl_contentPane = new GroupLayout(this.contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(this.list, GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE).addGroup(gl_contentPane.createSequentialGroup().addComponent(this.cmd, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE).addGap(18).addComponent(this.addr, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED, 232, Short.MAX_VALUE).addComponent(btnSend)).addGroup(gl_contentPane.createSequentialGroup().addComponent(this.lblCommand).addGap(53).addComponent(this.lblAdress)).addGroup(gl_contentPane.createSequentialGroup().addComponent(this.hexValue, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE).addGap(2).addComponent(this.btnAddHex).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.btnRemove, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE).addPreferredGap(ComponentPlacement.RELATED).addComponent(btnAddInt).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.intValue, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE))).addContainerGap()));
		this.list.addListSelectionListener(arg0 -> {
			this.btnRemove.setEnabled((this.list.getSelectedIndex() != -1) && (this.model.getSize() > 0));
		});
		this.btnRemove.addActionListener(e -> {
			if (this.list.getSelectedIndex() != -1) {
				this.data.remove(this.list.getSelectedIndex());
				this.model.remove(this.list.getSelectedIndex());
			}
		});
		this.btnRemove.setEnabled(false);
		this.intValue.setModel(new SpinnerNumberModel(0, 0, 255, 1));
		this.cmd.setModel(new SpinnerNumberModel(0, 0, 255, 1));
		this.addr.setModel(new SpinnerNumberModel(0, 0, 65535, 1));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(this.lblCommand).addComponent(this.lblAdress)).addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(this.cmd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(this.addr, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnSend)).addGap(18).addComponent(this.list, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE).addGap(18).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(this.hexValue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(this.intValue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnAddInt).addComponent(this.btnRemove).addComponent(this.btnAddHex)).addContainerGap(15, Short.MAX_VALUE)));
		this.contentPane.setLayout(gl_contentPane);

		this.setVisible(true);
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		try {
			this.frame.serialPort.closePort();
		} catch (SerialPortException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public final static int polynomial = 0x1021;

}
