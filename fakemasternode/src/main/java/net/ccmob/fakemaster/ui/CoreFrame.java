package net.ccmob.fakemaster.ui;

import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class CoreFrame extends JFrame {

	private JPanel							contentPane;
	public static CoreFrame			instance;
	public SerialPort						serialPort;
	private JComboBox<String>		comboBox	= new JComboBox<>();
	private SerialListener			listenerFrame;
	private CommandConstructor	constructorFame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {

				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

				CoreFrame frame = new CoreFrame();
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CoreFrame() {
		this.setTitle("Connection Settings");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 254, 79);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(this.contentPane);

		JLabel lblComport = new JLabel("Com Port");

		JButton btnOpen = new JButton("Open");
		btnOpen.addActionListener((e) -> {
			this.serialPort = new SerialPort(this.comboBox.getSelectedItem().toString());

			try {
				this.serialPort.openPort();
				this.serialPort.setParams(9600, 8, 1, SerialPort.PARITY_NONE);
				this.setVisible(false);
				this.constructorFame = new CommandConstructor(this);
				this.listenerFrame = new SerialListener(this);
				new ColorSlider(this);
			} catch (SerialPortException e1) {
				e1.printStackTrace();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(this.contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addComponent(lblComport).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.comboBox, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnOpen).addContainerGap(174, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(lblComport).addComponent(this.comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnOpen)).addContainerGap(237, Short.MAX_VALUE)));

		String[] ports = SerialPortList.getPortNames();
		for (String port : ports) {
			this.comboBox.addItem(port);
		}

		this.contentPane.setLayout(gl_contentPane);
	}
}
