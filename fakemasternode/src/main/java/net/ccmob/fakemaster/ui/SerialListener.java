package net.ccmob.fakemaster.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import jssc.SerialPortEvent;
import jssc.SerialPortException;

public class SerialListener extends JFrame {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;
	private JPanel						contentPane;
	private JTextArea					textArea					= new JTextArea();
	private CoreFrame					frame;

	/**
	 * Create the frame.
	 */
	public SerialListener(CoreFrame frame) {
		this.frame = frame;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 450, 300);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.setContentPane(this.contentPane);

		JScrollPane scrolpane = new JScrollPane(this.textArea);

		scrolpane.setAutoscrolls(true);

		this.contentPane.add(scrolpane, BorderLayout.CENTER);

		try {
			this.frame.serialPort.addEventListener((serialPortEvent) -> {
				try {
					byte[] bytes = this.frame.serialPort.readBytes();
					if (bytes == null) {
						return;
					}
					this.Append(new String(bytes));
				} catch (SerialPortException e) {
					e.printStackTrace();
				}
			}, SerialPortEvent.RXCHAR);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}

		this.setVisible(true);
	}

	public void Append(String s) {
		this.textArea.append(s);
	}

}
